/**
 * @file CameraControl.cpp
 * @date 12-Apr-2020
 */

#include "orx.h"
#include "CameraControl.h"

orxOBJECT *ship = orxNULL;
orxOBJECT *shipRed = orxNULL;

orxFLOAT fShipSpeed = 300;

void orxFASTCALL MoveShip(const orxCLOCK_INFO *_pstClockInfo) {

    orxVECTOR newPos;

    orxObject_GetPosition(ship, &newPos);
    if (orxInput_IsActive("MoveRight")) {
        newPos.fX += fShipSpeed * _pstClockInfo->fDT;
        orxObject_SetRotation(ship, 0.2);
    }
    else
    if (orxInput_IsActive("MoveLeft")) {
        newPos.fX -= fShipSpeed * _pstClockInfo->fDT;
        orxObject_SetRotation(ship, -0.2);
    }
    else {
        orxObject_SetRotation(ship, 0);
    }

    if (orxInput_IsActive("MoveUp")) {
        newPos.fY -= fShipSpeed * _pstClockInfo->fDT;
    }
    else
    if (orxInput_IsActive("MoveDown")) {
        newPos.fY += fShipSpeed * _pstClockInfo->fDT;
    }
    orxObject_SetPosition(ship, &newPos);
}

/** Update function, it has been registered to be called every tick of the core clock
 */
void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
    // Should quit?
    if (orxInput_IsActive("Quit"))
    {
        // Send close event
        orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
    }

    MoveShip(_pstClockInfo);

    if (orxInput_HasBeenActivated("ChangeTarget")) {
        if (cameraControl_GetTarget() == ship) {
            cameraControl_SetTarget(shipRed);
        }
        else {
            cameraControl_SetTarget(ship);
        }
    }

    if (orxInput_HasBeenActivated("DecreaseYOffset")) {
        cameraControl_SetOffsetY(cameraControl_GetOffsetY() - 20);
    }
    else
    if (orxInput_HasBeenActivated("IncreaseYOffset")) {
        cameraControl_SetOffsetY(cameraControl_GetOffsetY() + 20);
    }
    else
    if (orxInput_HasBeenActivated("DecreaseXOffset")) {
        cameraControl_SetOffsetX(cameraControl_GetOffsetX() - 20);
    }
    else
    if (orxInput_HasBeenActivated("IncreaseXOffset")) {
        cameraControl_SetOffsetX(cameraControl_GetOffsetX() + 20);
    }

    if (orxInput_HasBeenActivated("ToggleCameraDebug")) {
        cameraControl_SetShowDebug(!cameraControl_GetShowDebug());
    }
}

/** Init function, it is called when all orx's modules have been initialized
 */
orxSTATUS orxFASTCALL Init()
{

    // Create the viewport
    orxViewport_CreateFromConfig("MainViewport");

    orxMouse_ShowCursor(false);

    //initialize the camera control system, before creating objects
    cameraControl_Init();

    // Create the scene
    orxObject_CreateFromConfig("Scene");

    //create two ships and save references to them so we can
    //swap the main camera target when when we want
    ship = orxObject_CreateFromConfig("ShipObject");
    shipRed = orxObject_CreateFromConfig("ShipObjectRed");

   
    orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_HIGH);

    // Done!
    return orxSTATUS_SUCCESS;
}

/** Run function, it should not contain any game logic
 */
orxSTATUS orxFASTCALL Run()
{
    // Return orxSTATUS_FAILURE to instruct orx to quit
    return orxSTATUS_SUCCESS;
}

/** Exit function, it is called before exiting from orx
 */
void orxFASTCALL Exit()
{
    // Let Orx clean all our mess automatically. :)
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS orxFASTCALL Bootstrap()
{
    // Add a config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Set the bootstrap function to provide at least one resource storage before loading any config files
    orxConfig_SetBootstrap(Bootstrap);

    // Execute our game
    orx_Execute(argc, argv, Init, Run, Exit);

    // Done!
    return EXIT_SUCCESS;
}
