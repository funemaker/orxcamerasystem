#ifndef CAMERA_CONTROL_H
#define CAMERA_CONTROL_H
#include "orx.h"
#include <vector>
#include <iostream>

struct ObjectOfInterest {

    orxOBJECT *object;
    orxFLOAT FullInfluenceRadius;
    orxFLOAT PartialInfluenceRadius;

};

    void cameraControl_Init();

    //use setCurrentPosition to immediately move the camera positioner to
    //a specific location...good for teleporting a target (avoiding the camera following
    //over to the new location) or when moving a target to a start location after
    //creating the object
    void orxFASTCALL cameraControl_setCurrentPosition(orxVECTOR * _targetLocation);


    //updates camera position based on target influences
    void orxFASTCALL cameraControl_Update(const orxCLOCK_INFO * _pstClockInfo, void * _pContext);

    orxSTATUS orxFASTCALL ObjectEventHandler(const orxEVENT *_pstEvent);

    void orxFASTCALL cameraControl_SetOffsetX(orxFLOAT _x);
    void orxFASTCALL cameraControl_SetOffsetY(orxFLOAT _y);

    orxFLOAT orxFASTCALL cameraControl_GetOffsetX();
    orxFLOAT orxFASTCALL cameraControl_GetOffsetY();

    void static orxFASTCALL cameraControl_StaticUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pContext);
    
    void orxFASTCALL cameraControl_getTargetLocation(orxVECTOR *_targetLocation);

    void orxFASTCALL cameraControl_SetTarget(orxOBJECT *_cameraTarget);
    orxOBJECT* orxFASTCALL cameraControl_GetTarget();

    void orxFASTCALL cameraControl_SetShowDebug(bool _bShowDebug);
    bool orxFASTCALL cameraControl_GetShowDebug();

    void orxFASTCALL cameraControl_AddCamera(orxCAMERA *_camera);

    void orxFASTCALL cameraControl_AddObjectOfInterest(orxOBJECT * _object);

#endif /*CAMERA_CONTROL_H*/
