#include "CameraControl.h"

bool bShowDebug = false;

orxVECTOR vScreenSize;
orxVECTOR vFrustrumSize;

orxVECTOR vCurrentPosition;
orxVECTOR vCurrentVelocity;

orxFLOAT fDefaultFullInfluenceRadius = 200;
orxFLOAT fDefaultPartialInfluenceRadius = 400;

orxFLOAT fDefaultLagFactor = 0.8;

orxFLOAT fDefaultMaxVelocity = 600;

orxFLOAT vScreenToFrustrumRatio;
orxFLOAT fLagFactor = 0.3;

orxFLOAT fMaxVelocity = 600;

orxFLOAT fCameraOffsetX = 0;
orxFLOAT fCameraOffsetY = 0;

bool bHasCameraMinX, bHasCameraMinY, bHasCameraMaxX, bHasCameraMaxY = false;
orxFLOAT fCameraMinX, fCameraMinY, fCameraMaxX, fCameraMaxY = 0;

//the target for the camera...eventually need to upgrade
//to allow multiple target influence
orxOBJECT *ooCameraTarget = orxNULL;

//ObjectOfInterest sObjectOfInterest;//eventually this will be a vecctor
std::vector<ObjectOfInterest> objectsOfInterest;

//this is the object the cameras will parent to
orxOBJECT *CameraPositionerObject = orxNULL;
orxOBJECT *CameraCrossHairs = orxNULL;

void cameraControl_Init()
{
    // Display a small hint in console
    orxLOG("\n* This project demonstrates the CameraControl object used to produce "
        "\n* camera lag and smooth transition between the main camera object and"
        "\n* a list of objects of interest which influence the location of the camera"
        "\n* based on influence radius settings.  See the entries in CameraSystem.ini "
        "\n* for setting up the camera control as well the main target and objects of interest."
        "\n* Mouse left click to switch the main camera focus between the two ships."
        "\n* Space bar to toggle the visual debugging.  WASD to move around."
        "\n* Arrow keys to increase/decrease offset.");

    orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), cameraControl_Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
    orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, ObjectEventHandler);
    orxConfig_PopSection();

    //setup the crosshair section
    
    orxConfig_PushSection("CameraControl_CrossHairs");  //creates the section if it doesn't exist
    orxConfig_SetString("Graphic", "@");
    orxConfig_SetString("Texture", "CameraControlCrossHairs.png");
    orxConfig_SetString("Pivot", "Center");
    orxVECTOR vPos = orxVECTOR_0;
    vPos.fZ = -0.5f;
    orxConfig_SetVector("Position", &vPos);
    orxConfig_SetFloat("Alpha", 0.25f);
    orxConfig_PopSection();

    //create the positioner object - camera gets parented to this
    CameraPositionerObject = orxObject_CreateFromConfig("CameraControl");
    orxASSERT(CameraPositionerObject);

    orxConfig_PushSection("CameraControl");

    //get max velocity and camera speed factor from ini
    fMaxVelocity = fDefaultMaxVelocity;
    if (orxConfig_HasValue("MaxVelocity")) {
        fMaxVelocity = orxConfig_GetFloat("MaxVelocity");
    }
    
    bShowDebug = orxConfig_GetBool("ShowDebug");
    //set debug state
    cameraControl_SetShowDebug(bShowDebug);

    //get max camera lag factor from ini and get into suitable range
    orxFLOAT fTmp = 1.0f - fDefaultLagFactor;
    if (orxConfig_HasValue("LagFactor")) {
        fTmp = 1.0f - orxMIN(orxConfig_GetFloat("LagFactor"), 1.0f);
    }
    fLagFactor = (fTmp * 20) + 0.1F; //get into range of .1 - 20.1 magic range

    //add cameras (sets them as children of the position object)
    for (orxS32 i = 0, count = orxConfig_GetListCount("CameraList"); i < count; i++)
    {
        cameraControl_AddCamera(orxCamera_Get(orxConfig_GetListString("CameraList", i)));
    }

    if (orxConfig_HasValue("CameraMinX")) {
        fCameraMinX = orxConfig_GetFloat("CameraMinX");
        bHasCameraMinX = true;
    }
    if (orxConfig_HasValue("CameraMaxX")) {
        fCameraMaxX = orxConfig_GetFloat("CameraMaxX");
        bHasCameraMaxX = true;
    }
    if (orxConfig_HasValue("CameraMinY")) {
        fCameraMinY = orxConfig_GetFloat("CameraMinY");
        bHasCameraMinY = true;
    }
    if (orxConfig_HasValue("CameraMaxY")) {
        fCameraMaxY = orxConfig_GetFloat("CameraMaxY");
        bHasCameraMaxY = true;
    }

    //initialize position and velocity
    orxObject_GetPosition(CameraPositionerObject, &vCurrentPosition);
    vCurrentVelocity = orxVECTOR_0;

    //need screen and frustrum size in order to correctly size the radius on the
    //debug circles drawn around the objects of interest
    orxDisplay_GetScreenSize(&vScreenSize.fX, &vScreenSize.fY);
    orxAABOX frustrum;
    orxCamera_GetFrustum(orxCamera_Get("MainCamera"), &frustrum);
    vFrustrumSize.fX = frustrum.vBR.fX - frustrum.vTL.fX;
    vFrustrumSize.fY = frustrum.vBR.fY - frustrum.vTL.fY;
    vScreenToFrustrumRatio = vScreenSize.fX / vFrustrumSize.fX;

}

void orxFASTCALL cameraControl_getTargetLocation(orxVECTOR *_targetLocation)
{
    //this function determines the target location by looking at the objects
    //of interest and determining if any are within range and applying influence
    //as needed.

    orxFLOAT fDistanceToOI;
    orxVECTOR vMainTargetPos;
    orxVECTOR vOiPos;
    orxVECTOR vTargetToInterest;

    orxObject_GetPosition(ooCameraTarget, &vMainTargetPos);

    //if no objects of interest, this will be default
    orxObject_GetPosition(ooCameraTarget, _targetLocation);
    //apply offset
    _targetLocation->fX += fCameraOffsetX;
    _targetLocation->fY += fCameraOffsetY;

    if (bShowDebug) {
        for (auto& objectOfInterest : objectsOfInterest) {

            //draw some blue circles to represent the influence radius
            //for visual debugging

            //make red
            orxRGBA color;
            color.u8R = 0;
            color.u8G = 0;
            color.u8B = 255;

            orxObject_GetPosition(objectOfInterest.object, &vOiPos);

            orxVECTOR screenPos;
            orxRender_GetScreenPosition(&vOiPos, orxNULL, &screenPos);
            orxDisplay_DrawCircle(&screenPos, objectOfInterest.PartialInfluenceRadius * vScreenToFrustrumRatio, color, false);
            orxDisplay_DrawCircle(&screenPos, objectOfInterest.FullInfluenceRadius * vScreenToFrustrumRatio, color, false);

            //make blue
            color.u8B = 0;
            color.u8R = 255;

            orxVECTOR startPointL, endPointL, startPointR, endPointR;

            orxVECTOR worldPos;
            worldPos.fX = fCameraMinX;
            worldPos.fY = fCameraMinY;
            orxRender_GetScreenPosition(&worldPos, orxNULL, &startPointL);
            if (!bHasCameraMinY) {
                startPointL.fY = 0;
            }
            if (!bHasCameraMinX) {
                startPointL.fX = 0;
            }

            worldPos.fX = fCameraMinX;
            worldPos.fY = fCameraMaxY;
            orxRender_GetScreenPosition(&worldPos, orxNULL, &endPointL);
            if (!bHasCameraMaxY) {
                endPointL.fY = vScreenSize.fY;
            }
            if (!bHasCameraMinX) {
                endPointL.fX = 0;
            }

            worldPos.fX = fCameraMaxX;
            worldPos.fY = fCameraMinY;
            orxRender_GetScreenPosition(&worldPos, orxNULL, &startPointR);
            if (!bHasCameraMinY) {
                startPointR.fY = 0;
            }
            if (!bHasCameraMaxX) {
                startPointR.fX = vScreenSize.fX;
            }

            worldPos.fX = fCameraMaxX;
            worldPos.fY = fCameraMaxY;
            orxRender_GetScreenPosition(&worldPos, orxNULL, &endPointR);
            if (!bHasCameraMaxY) {
                endPointR.fY = vScreenSize.fY;
            }
            if (!bHasCameraMaxX) {
                endPointR.fX = vScreenSize.fX;
            }

            //draw the lines as needed to represent the min/max values
            if (bHasCameraMinX) {
                orxDisplay_DrawLine(&startPointL, &endPointL, color);
            }
            if (bHasCameraMaxX) {
                orxDisplay_DrawLine(&startPointR, &endPointR, color);
            }
            if (bHasCameraMinY) {
                orxDisplay_DrawLine(&startPointL, &startPointR, color);
            }
            if (bHasCameraMaxY) {
                orxDisplay_DrawLine(&endPointL, &endPointR, color);
            }
        }
    }

    for (auto& objectOfInterest : objectsOfInterest) {

        orxObject_GetPosition(objectOfInterest.object, &vOiPos);
        orxVector_Sub(&vTargetToInterest, &vMainTargetPos, &vOiPos);
        fDistanceToOI = orxVector_GetSize(&vTargetToInterest);

        if (fDistanceToOI > objectOfInterest.PartialInfluenceRadius) {
            //not in this object's influence, so set _targetLocation to
            //the main camera target
            orxObject_GetPosition(ooCameraTarget, _targetLocation);
            //apply offset
            _targetLocation->fX += fCameraOffsetX;
            _targetLocation->fY += fCameraOffsetY;
        }
        else {
            if (fDistanceToOI > objectOfInterest.FullInfluenceRadius) {
                //here we are in the inbetween zone, so get a lerp of the two locations
                //based on how far into the zone we are
                orxFLOAT fZoneWidth = objectOfInterest.PartialInfluenceRadius -
                    objectOfInterest.FullInfluenceRadius;
                orxFLOAT fDistanceIn = objectOfInterest.PartialInfluenceRadius -
                    fDistanceToOI;
                orxFLOAT fZoneDepth = fDistanceIn / fZoneWidth;
                orxVector_Lerp(_targetLocation, &vMainTargetPos, &vOiPos, fZoneDepth);

                //apply offset with lerp so it is not applied as offset on the object of interest
                _targetLocation->fX += orxLERP(fCameraOffsetX, 0, fZoneDepth);
                _targetLocation->fY += orxLERP(fCameraOffsetY, 0, fZoneDepth);

                //not allowing for multiple object of interests influence
                //just breaking after finding the first
                break;

            }
            else {
                //inside the full influence radius, so target is the location of the object
                //of interest
                *_targetLocation = vOiPos;

                //not allowing for multiple objects of interest influence
                //just breaking after finding the first
                break;
            }
        }
    }
}


void orxFASTCALL cameraControl_setCurrentPosition(orxVECTOR * _targetLocation)
{
    vCurrentPosition = *_targetLocation;
}

void orxFASTCALL cameraControl_Update(const orxCLOCK_INFO * _pstClockInfo, void * _pContext)
{
    //do nothing if target has not been set
    if (ooCameraTarget == orxNULL) {
        return;
    }

    orxVECTOR vTargetPosition;
    orxVECTOR vDesiredVelocity;
    orxVECTOR vVectorToTarget;
    orxVECTOR vDirectionToTarget;
    orxFLOAT fDistanceToTarget;
    orxVECTOR vDistanceTravelled;
    orxVECTOR vNewPosition;

    //start by getting our targets position

    cameraControl_getTargetLocation(&vTargetPosition);

    //apply limits - (offsets already applied in getTargetLocation due to influence of objects of interest)

    if (bHasCameraMaxX) {
        vTargetPosition.fX = orxMIN(vTargetPosition.fX, fCameraMaxX);
    }
    if (bHasCameraMaxY) {
        vTargetPosition.fY = orxMIN(vTargetPosition.fY, fCameraMaxY);
    }
    if (bHasCameraMinX) {
        vTargetPosition.fX = orxMAX(vTargetPosition.fX, fCameraMinX);
    }
    if (bHasCameraMinY) {
        vTargetPosition.fY = orxMAX(vTargetPosition.fY, fCameraMinY);
    }

    //get a vector from current position to our target
    orxVector_Sub(&vVectorToTarget, &vTargetPosition, &vCurrentPosition);

    //get our distance to target
    fDistanceToTarget = orxVector_GetSize(&vVectorToTarget);

    //get a direction vector to the target
    orxVector_Normalize(&vDirectionToTarget, &vVectorToTarget);

    //set a speed factor based on distance and lag factor
    orxFLOAT speedFactor = fDistanceToTarget * fLagFactor;

    //cap the speed factor
    if (speedFactor > fMaxVelocity) {
        speedFactor = fMaxVelocity;
    }

    //setup our desired velocity based on the speed factor and direction to the target
    orxVector_Mulf(&vDesiredVelocity, &orxVECTOR_1, speedFactor);
    orxVector_Mul(&vDesiredVelocity, &vDesiredVelocity, &vDirectionToTarget);

    //use a lerp to smoothly change between current velocity and desired velocity
    //using a factor based on distance to target and time elapsed. As we get very close to the target
    //the new velocity has more influence.  This protects against shooting past the target
    //and "orbiting" the target.  Maybe there is a better way than this, but it is working
    //for now
    orxVector_Lerp(&vCurrentVelocity, &vCurrentVelocity, &vDesiredVelocity, (orxMIN(_pstClockInfo->fDT * (1000 / (fDistanceToTarget + 1)), 1.0f)));

    //get how far the camera should trave based on velocity and time elapsed
    orxVector_Mulf(&vDistanceTravelled, &vCurrentVelocity, _pstClockInfo->fDT);

    //add distance travelled to current position
    orxVector_Add(&vNewPosition, &vCurrentPosition, &vDistanceTravelled);

     //update our object with new position
    orxObject_SetPosition(CameraPositionerObject, &vNewPosition);

    //save current position for next time
    vCurrentPosition = vNewPosition;

}

void orxFASTCALL cameraControl_SetTarget(orxOBJECT * _cameraTarget)
{
    ooCameraTarget = _cameraTarget;
}

orxOBJECT *orxFASTCALL cameraControl_GetTarget()
{
    return ooCameraTarget;
}

void orxFASTCALL cameraControl_SetShowDebug(bool _bShowDebug)
{
    bShowDebug = _bShowDebug;

    //create or kill the crosshair object as needed
    if (bShowDebug) {
        CameraCrossHairs = orxObject_CreateFromConfig("CameraControl_CrossHairs");
        orxObject_SetParent(CameraCrossHairs, CameraPositionerObject);
    }
    else {
        if (CameraCrossHairs != orxNULL) {
            orxObject_SetLifeTime(CameraCrossHairs, 0);
            CameraCrossHairs = orxNULL;
        }
    }
}

bool orxFASTCALL cameraControl_GetShowDebug()
{
    return bShowDebug;
}

void orxFASTCALL cameraControl_AddCamera(orxCAMERA * _camera)
{
    //just sets the camera's parent to the positioner
    orxCamera_SetParent(_camera, CameraPositionerObject);
}

void orxFASTCALL cameraControl_AddObjectOfInterest(
    orxOBJECT * _object)
{
    orxASSERT(_object);

    //look up the influence radius settings in the config file
    orxConfig_PushSection(orxObject_GetName(_object));

    //create a structure to a reference to the object along with its
    //influence settings
    ObjectOfInterest tmpOOI;
    tmpOOI.object = _object;
    //should set defaults and only pull these if found

    orxFLOAT fTmp = fDefaultFullInfluenceRadius;

    if (orxConfig_HasValue("FullInfluenceRadius")) {
        fTmp = orxConfig_GetFloat("FullInfluenceRadius");
    }
    tmpOOI.FullInfluenceRadius = fTmp;

    fTmp = fDefaultPartialInfluenceRadius;

    if (orxConfig_HasValue("PartialInfluenceRadius")) {
        fTmp = orxConfig_GetFloat("PartialInfluenceRadius");
    }
    tmpOOI.PartialInfluenceRadius = fTmp;

    //don't allow messed up entries in the ini
    orxASSERT(tmpOOI.PartialInfluenceRadius > tmpOOI.FullInfluenceRadius);

    //add to our collection of objects of interest
    objectsOfInterest.push_back(tmpOOI);

    orxConfig_PopSection();
}

orxSTATUS orxFASTCALL ObjectEventHandler(const orxEVENT *_pstEvent) {

    const orxSTRING value;
    switch (_pstEvent->eID) {

    case orxOBJECT_EVENT_CREATE:

        orxConfig_PushSection(orxObject_GetName(orxOBJECT(_pstEvent->hSender)));

        value = orxConfig_GetString("Control");

        if (orxString_Compare(value, "MainTarget") == 0) {
            //this is our main target, so set it
            cameraControl_SetTarget(orxOBJECT(_pstEvent->hSender));
            fCameraOffsetX = orxConfig_GetFloat("CameraOffsetX");
            fCameraOffsetY = orxConfig_GetFloat("CameraOffsetY");
            vCurrentPosition.fX += fCameraOffsetX;
            vCurrentPosition.fY += fCameraOffsetY;
        }
        else
            if (orxString_Compare(value, "ObjectOfInterest") == 0) {
                //add to our objects of interest list

                cameraControl_AddObjectOfInterest(orxOBJECT(_pstEvent->hSender));

            }
        orxConfig_PopSection();
        break;

    case orxOBJECT_EVENT_DELETE:

        //run through objects of interest and if it is there, delete it

        for (auto itr = objectsOfInterest.begin(); itr != objectsOfInterest.end();)
        {
            if (ObjectOfInterest(*itr).object == orxOBJECT(_pstEvent->hSender)) {
                itr = objectsOfInterest.erase(itr);
            }
            else {
                ++itr;
            }
        }
        if (ooCameraTarget == _pstEvent->hSender) {

            ooCameraTarget = orxNULL;
        }
        break;
    }
    return orxSTATUS_SUCCESS;
}

void orxFASTCALL cameraControl_SetOffsetX(orxFLOAT _x)
{
    fCameraOffsetX = _x;
}

void orxFASTCALL cameraControl_SetOffsetY(orxFLOAT _y)
{
    fCameraOffsetY = _y;
}

orxFLOAT orxFASTCALL cameraControl_GetOffsetX()
{
    return fCameraOffsetX;
}

orxFLOAT orxFASTCALL cameraControl_GetOffsetY()
{
    return fCameraOffsetY;
}
